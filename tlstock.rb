#!/usr/bin/env ruby
require 'sequel'
require 'openssl'
require 'socket'

require './lib/provider_aws.rb'
require './lib/provider_gcp.rb'

class Stock
  def initialize
    database_file = 'certificates.db'
    create_schema(database_file) if not File.exist?(database_file)
    @db = Sequel.sqlite('certificates.db')

    @certificates = @db[:certificates]
    @deployments = @db[:deployments]
  end

  def create_schema(file)
    db = Sequel.sqlite('certificates.db')
    db.create_table :certificates do
      primary_key :id
      String :serial, index: true, unique: true
      String :issuer
      String :issuer_cn
      String :not_before
      String :not_after
      String :subject
      String :commonName
    end

    db.create_table :deployments do
      primary_key :id
      Integer :certificate_id
      String :location
      String :source
      foreign_key [:certificate_id], :certificates, name: 'deployments_certificates_id_fkey'
    end
  end

  def get_all_certificates
    @certificates.all
  end

  def get_by_serial(serial)
    @certificates.where(serial: serial)
  end

  def deployments(serial)
    id = get_by_serial(serial).first
    if id.count != 0 then
      return @deployments.where(certificate_id: id[:id])
    else
      return []
    end
  end

  def common_name(subject)
    if subject.is_a? String then
      subject.split(/=/)[-1]
    else
      subject
    end
  end

  def update_database(cert, source, deployment)
    serial = cert.is_a?(Hash) ? cert[:serial].to_s.gsub(/:/,'').upcase : cert.serial
    subject = cert.is_a?(Hash) ? cert[:subject] : cert.subject
    not_before = cert.is_a?(Hash) ? cert[:not_before] : cert.not_before
    not_after = cert.is_a?(Hash) ? cert[:not_after] : cert.not_after
    issuer = cert.is_a?(Hash) ? cert[:issuer] : cert.issuer.to_s
    id = get_by_serial(serial)
    if id.count == 0 then
      puts "Adding new certificate to database (serial: #{serial}, subject: #{subject})"
      id = @certificates.insert(serial: serial, subject: subject, not_before: not_before, not_after: not_after, issuer: issuer, issuer_cn: common_name(issuer), commonName: common_name(subject))
    else
      id = id.first[:id]
      puts "Certificate with serial #{serial} already in database (id: #{id})"
    end

    deployments = @deployments.where(certificate_id: id).where(location: deployment)
    if deployments.count == 0 then
      puts "Discovered new deployment: #{serial} AT #{deployment}, source: #{source}"
      @deployments.insert(certificate_id: id, source: source, location: deployment)
    else
      puts "Deployment of #{serial} at #{deployment} already registered"
    end
  end
end

class Certificate
  def initialize()

  end

  def cert_from_ip(domain, remote_port = 443, sni = false)
    begin
      tcp_client = TCPSocket.new domain, remote_port
    rescue SocketError => e
      puts e
      puts "Failed to connect to: #{domain}:#{port}, SNI: #{sni.to_s} (#{e.message})."
      return false
    end
    ctx = OpenSSL::SSL::SSLContext.new
    @remote_ip = tcp_client.peeraddr.last
    @ssl_client = OpenSSL::SSL::SSLSocket.new tcp_client, ctx
    @ssl_client.hostname=domain if sni
    @ssl_client.connect
    @certificate = @ssl_client.peer_cert
  end

  def cert_from_string(string)
    @certificate = OpenSSL::X509::Certificate.new string
  end

  def remote_ip
    @remote_ip
  end

  def serial
    @certificate.serial.to_s(16)
    @certificate.serial.to_s(16)
  end

  def issuer
    @certificate.issuer
  end

  def subject
    @certificate.subject.to_s
  end

  def not_after
    @certificate.not_after
  end

  def not_before
    @certificate.not_before
  end

  def expired?
    return Time.now-self.not_after > 0 ? true : false
  end

  def days_left
    return ((self.not_after-Time.now)/62/60/24).to_i
  end
end

stock = Stock.new

File.foreach('domains.txt') do |domain|
  domain.chomp!
  cert = Certificate.new
  cert.cert_from_ip(domain, 443, true)
  puts "Checking #{domain} deployed on #{cert.remote_ip}"
  puts "Certificate for #{domain} expired #{cert.days_left.abs} days ago!" if cert.expired?
  puts "Certificate for #{domain} expires in #{cert.days_left} days!" if cert.days_left < 30 && cert.days_left > 0
  p cert
  stock.update_database(cert, "file:domains.txt", cert.remote_ip)
end

aws = AWS.new
aws.elb_list_ips.load_balancer_descriptions.each do |lb|
  hostname = lb.to_h[:dns_name]
  lb.listener_descriptions.select { |listener| listener.to_h[:listener][:protocol] == 'HTTPS'}.each do |listener|
    listener = listener.to_h[:listener]
    port = listener[:load_balancer_port]
    cert = Certificate.new
    cert.cert_from_ip(hostname, 443, false)
    stock.update_database(cert, "aws:elb:#{listener[:ssl_certificate_id]}", hostname)
  end
end

aws.acm_list_certificates.to_h[:certificate_summary_list].each do |cert|
  cert = aws.acm_get_certificate(cert[:certificate_arn]).to_h[:certificate]
  source = "aws:acm"
  deployment = cert[:certificate_arn]
  stock.update_database(cert, source, deployment)
end

gcp = GCP.new
gcp.list_certificates.each do |certificate|
  cert = Certificate.new
  cert.cert_from_string(certificate['certificate'])
  source = "gcp:certificates"
  stock.update_database(cert, source, certificate['name'])
end

(0..10).each do
  print "="
end
puts
puts "Certificates in database:"
stock.get_all_certificates.each do |certificate|
  puts "Subject: #{certificate[:commonName]} Issuer: #{certificate[:issuer_cn]}"
  puts "\tDeployments:"
  stock.deployments(certificate[:serial]).each do |deployment|
    puts "\t - #{deployment[:location]} (source: #{deployment[:source]})"
  end
end
