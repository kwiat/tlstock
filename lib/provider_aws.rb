require 'aws-sdk'

class AWS
  def initialize()

  end

  def elb_list_ips
    aws = Aws::ElasticLoadBalancing::Client.new()
    elbs = aws.describe_load_balancers
  end

  def acm_list_certificates
    aws = Aws::ACM::Client.new
    certs = aws.list_certificates
  end

  def acm_get_certificate(arn)
    aws = Aws::ACM::Client.new
    cert = aws.describe_certificate({
        certificate_arn: arn
    })
  end

  def route53_get_records(zoneid)
    aws = Aws::Route53::Client.new
    hosts = aws.list_resource_record_sets({
        hosted_zone_id: zoneid
    })
  end
end
